import org.w3c.dom.ls.LSOutput;

public class StringAnalyzer {
    public static void main(String[] args) {
        String argument = args[0];
        if (argument.matches("[A-Za-z0-9+\\-,_+]*")) {
            char[] symbol = {'A'};
            String upperCaseA = new String(symbol);
            System.out.println(argument.replaceAll("a", "bcd") +
                    argument.replaceAll("a", upperCaseA));
            System.out.println(argument.length());
            int half = argument.length() / 2;
            System.out.println(argument.substring(half, argument.length()) +
                    argument.substring(0, half));
        } else {
            System.out.println("Invalid input.");
        }
    }
}
