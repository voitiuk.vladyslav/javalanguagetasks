import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WWS2 {
    public static void main(String[] args) throws IOException {
        String input = "HereIsSomeSymmbols";
        char arrayInput[] = input.toCharArray();
        String s1 = input.substring(0, 6);
        String s2 = input.substring(6, 12);
        String s3 = input.substring(12, 18);
        System.out.printf("1/3= %s\n2/3= %s\n3/3= %s\n", s1, s2, s3);
        System.out.println(s2 + s3 + s1);
        System.out.println(s2 + s1 + s3);
    }
}
