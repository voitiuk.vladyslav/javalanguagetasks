import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WWS3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Input your string: ");
        String input = reader.readLine();
        System.out.print("Input k number: ");
        String number = reader.readLine();
        int k = Integer.parseInt(number);
        int lastIndex = input.length();
        System.out.println(input.substring(k, lastIndex) + input.substring(0, k));
    }
}
