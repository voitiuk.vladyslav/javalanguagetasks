import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WWS1 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        System.out.println(input.length());
        System.out.println(input.replaceAll(" ", "\t"));
        System.out.print("Input k symbol: ");
        String k = reader.readLine();
        System.out.println(k + input.substring(1, input.length()).replaceAll(" ", "\t"));
    }
}
