import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Like {
    public static void main(String[] args) throws Exception {
        String result, x, y;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Input X variable: ");
        x = reader.readLine();
        System.out.print("Input Y variable: ");
        y = reader.readLine();
        String s3 = "I like " + x + " because of " + y;
        result = s3;
        System.out.println(result);
    }
}
