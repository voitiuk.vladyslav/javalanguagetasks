import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Gauge {
    public static void main(String[] args) throws Exception {
        String input;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        input = reader.readLine();
        int inputLength = input.length();
        System.out.println(inputLength);
        System.out.println(input.charAt(inputLength - 1) + input.substring(1, inputLength - 1) + input.charAt(0));
        int divide = inputLength % 2;
        boolean evenOrOddLength = divide == 0;
        System.out.println(evenOrOddLength);
    }
}
