public class DoWhileDemo {
    public static void main(String[] args) {
        int count = 1000;
        int array[] = new int[1000];
        int decrement = 0;
        do {
            decrement = decrement + 2;
            array[array.length - decrement] = count;
            int arrayElement = array.length - decrement;
            System.out.println("Count is:" + arrayElement + " = " + array[array.length - decrement]);
            count--;
        } while (decrement != 1000);
    }
}
