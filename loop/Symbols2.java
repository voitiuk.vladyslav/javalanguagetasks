import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Symbols2 {
    public static void main(String[] args) throws IOException {
        String argument = args[0];
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        char toChar = input.charAt(0);
        char a = 'a';
        char o = 'o';
        char e = 'e';
        if (toChar == a) {
            System.out.println(argument);
        } else if (toChar == e) {
            char[] array = new char[argument.length()];
            array = argument.toCharArray();
            int counter = 0;
            while (counter < array.length - 1) {
                counter = counter + 1;
                System.out.print(array[counter]);
                counter++;
            }
        } else if (toChar == o) {
            char[] array = new char[argument.length()];
            array = argument.toCharArray();
            int counter = 0;
            while (counter <= array.length) {
                System.out.print(array[counter]);
                counter = counter + 2;
            }
        } else {
            System.out.println("Enter one of the next characters if you want to see result: a, o, e");
        }
    }
}

