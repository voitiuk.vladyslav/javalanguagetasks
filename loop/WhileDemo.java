public class WhileDemo {
    public static void main(String[] args) {
        int count = 2000;
        int array[] = new int[1000];
        int decrement = 0;
        while (decrement != 1000) {
            decrement = decrement + 2;
            array[array.length - decrement] = count;
            int arrayElement = array.length - decrement;
            System.out.println("Count is:" + arrayElement + " = " + array[array.length - decrement]);
            count--;
        }
        System.out.println(array[0]);
    }
}
