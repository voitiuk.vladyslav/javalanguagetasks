public class Sum {
    public static void main(String[] args) {
        int sum = 0;
        int array[] = new int[]{12, 3, 6, 9, 55, 2, 1, 8, 5, 7};
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 12 || array[i] == 7) {
                continue;
            }
            sum += array[i];
        }
        System.out.println(sum);
    }
}
