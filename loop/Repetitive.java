public class Repetitive {
    public static void main(String[] args) {
        int array[] = new int[]{12, 13, 5, 7, 6, 5, 6, 12, 12};
        int maxCounter = 0;
        int arrayElement = 0;
        for (int i = 0; i < array.length; i++) {
            int counter = 1;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    counter++;
                }
            }
            if (maxCounter < counter) {
                maxCounter = counter;
                arrayElement = array[i];
            }
        }
        System.out.println(arrayElement);
    }
}
