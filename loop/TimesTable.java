import java.util.Arrays;

public class TimesTable {
    public static void main(String[] args) {
        int array1[] = new int[15];
        int array2[] = new int[15];
        int arrayMultiplicationResult[] = new int[15];
        final int range = 7;
        for (int i = 0; i < 15; i++) {
            int a = (int) (Math.random() * range) + 2;
            int b = (int) (Math.random() * range) + 2;
            if (a > 1 && a < 10 && b > 1 && b < 10) {
                int temp = 0;
                temp = a * b;
                arrayMultiplicationResult[i] = temp;
                array1[i] = a;
                array2[i] = b;
                for (int k = 0; k <= i; k++) {
                    if (String.valueOf(arrayMultiplicationResult[i]) ==
                            String.valueOf(arrayMultiplicationResult[k])) {
                        i--;
                    }
                }
            }
        }
        for (int j = 0; j < array1.length; j++) {
            String result = String.valueOf(array1[j]) + "*" + String.valueOf(array2[j]);
            System.out.println(result);
        }
    }
}


