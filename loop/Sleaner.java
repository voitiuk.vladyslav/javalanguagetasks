public class Sleaner {
    public static void main(String[] args) {
        String input = args[0];
        final String space = "\\s+";
        final String tab = "\\t+";
        final String dolla = "\u0024";
        input = input.replaceAll(space, "");
        input = input.replaceAll(tab, "");
        System.out.println(input.charAt(0) + dolla + input.substring(1));
    }
}
