public class ForArrayRandomDemo {
    public static void main(String[] args) {
        final double MAX_RND = 1000;
        int[] a = new int[100];
        for (int i = 0; i < a.length; i++) {
            double rnd = Math.random();
            a[i] = (int) (rnd * MAX_RND);
        }
        int sum = 0;
        for (int i = a.length / 2; i >= 0; i--) {
            if (a[i] > MAX_RND / 2) {
                sum += a[i];
            }
        }
        System.out.println(sum);
    }
}
