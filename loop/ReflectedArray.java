import java.util.Arrays;

public class ReflectedArray {
    public static void main(String[] args) {
        String argument = args[0];
        int input = Integer.parseInt(argument);
        int array[] = new int[input];
        int counter1 = 0;
        int counter2 = array.length / 2 - 1;
        int counter3 = array.length / 2;
        for (int i = 0; i < array.length; i++) {
            if (array.length % 2 == 0) { //With even number divide array conditionally by 2 independent arrays
                while (i < array.length / 2) {
                    array[i] = counter2 - i;
                    i++;
                }
                array[i] = counter1;
                counter1++;
            } else if (array.length % 2 != 0) { //With odd number beginning from middle index
                while (i < array.length / 2) {
                    array[i] = array.length / 2 - counter1;
                    counter1++;
                    i++;
                }
                array[i] = array.length / 2 - counter3;
                counter3--;
            }
        }
        System.out.println(Arrays.toString(array));
    }
}