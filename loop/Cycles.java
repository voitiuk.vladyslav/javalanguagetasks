public class Cycles {
    public static void main(String[] args) {
        String argument = args[0];
        int numbers = Integer.parseInt(argument);
        for (int i = 0; i <= numbers; i++) {
            System.out.println(i);
        }
        System.out.println();
        for (int i = 0; i <= numbers; i++) {
            int j = i % 3;
            if (j == 0) {
                System.out.println(i);
            }
        }
    }
}
