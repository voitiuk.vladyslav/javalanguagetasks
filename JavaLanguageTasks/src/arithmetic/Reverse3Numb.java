package arithmetic;

public class Reverse3Numb {

    public static void main(String[] args) {
        String s = args[0];
        int number = Integer.parseInt(s);
        int last_num, sum = 0;
        while (number > 0) {
            last_num = number % 10;
            number /= 10;
            sum = sum * 10 + last_num;
        }
        System.out.println("Reversed number:" + sum);
    }

}
