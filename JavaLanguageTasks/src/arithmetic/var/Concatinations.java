package arithmetic.var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Concatinations {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s1, s2, s3;
        System.out.print("Input text for the first variable: ");
        s1 = reader.readLine();
        System.out.print("Input text for the second variable: ");
        s2 = reader.readLine();
        System.out.print("Input text for the third variable: ");
        s3 = reader.readLine();

        System.out.println(s1 + s3);
        System.out.println(s3 + s2 + s1);
        System.out.println(s1 + s2 + s3);
    }

}
