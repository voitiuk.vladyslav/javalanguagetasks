package arithmetic.var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class RevInput {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input1;
        String input2;
        String input3;
        System.out.print("Input first digit: ");
        input1 = reader.readLine();
        int firstDigit = Integer.parseInt(input1);
        System.out.print("Input second digit: ");
        input2 = reader.readLine();
        int secondDigit = Integer.parseInt(input2);
        System.out.print("Input third digit: ");
        input3 = reader.readLine();
        int thirdDigit = Integer.parseInt(input3);
        System.out.println(thirdDigit + "\n" + secondDigit + "\n" + firstDigit);
    }

}
