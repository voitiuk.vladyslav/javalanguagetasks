package arithmetic.var;

public class Vars {

    public static void main(String[] args) {
        final String myname = "Vlad";
        byte age = 27;
        final short earthDiameter = 12742;
        byte outsideTemperature = 11;
        int notDate = 27052020;
        final double electronMass = 9.10938356 * Math.pow(10, -31);
        double CanisMajorDwarfGalaxy = 236 * Math.pow(10, 14);
    }

}
