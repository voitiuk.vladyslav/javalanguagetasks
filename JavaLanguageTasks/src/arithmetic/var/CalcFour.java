package arithmetic.var;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CalcFour {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s1, s2;
        int digit1, digit2;
        System.out.print("Input first digit: ");
        s1 = reader.readLine();
        digit1 = Integer.parseInt(s1);
        System.out.print("Iput second digit: ");
        s2 = reader.readLine();
        digit2 = Integer.parseInt(s2);
        System.out.println("Summ: " + (digit1 + digit2));
        System.out.println("Subtraction: " + (digit1 - digit2));
        System.out.println("Multiplication: " + (digit1 * digit2));
        System.out.println("Division: " + (digit1 / digit2));
    }

}
