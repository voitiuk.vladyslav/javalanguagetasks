package arithmetic.logic;

public class Lohica {

    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        boolean c = false;
        boolean result1, result2, result3, result4, result5, result6, result7;
        result1 = a || b;
        result2 = a && b;
        result3 = b && c;
        result4 = !a && b;
        result5 = a || !b;
        result6 = a || b && c;
        result7 = a || (!(b && c));
        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
        System.out.println("Check class:");
        System.out.println(a);
        System.out.println(b);
    }

    public class Check {
        int x = 2;
        int y = 3;
        boolean a = x * x * x - y * y <= 0;
        boolean b = (!(x * y < 1)) && (y > x);

    }

}