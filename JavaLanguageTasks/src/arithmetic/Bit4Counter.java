package arithmetic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Bit4Counter {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input, s1;
        int digit;
        System.out.println("Input natural number from 0 to 15:");
        input = reader.readLine();
        digit = Integer.parseInt(input);
        s1 = Integer.toBinaryString(digit);
        int count = s1.replaceAll("0", "").length();
        System.out.println("Number of unit bits = " + count);
    }

}
