package arithmetic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Intsqr {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String d1;
        System.out.print("Input the digit: ");
        d1 = reader.readLine();
        int number = Integer.parseInt(d1);
        System.out.println(number * number);
    }

}
