package arithmetic;

public class TDistance {

    public static void main(String[] args) {
        byte bmw = 50;
        byte audi = 30;
        byte distance = 80;
        byte t = 10; // distamce between cars after t hours
        int result = distance + (bmw + audi) * t;
        System.out.println(result + " km");
    }

}
