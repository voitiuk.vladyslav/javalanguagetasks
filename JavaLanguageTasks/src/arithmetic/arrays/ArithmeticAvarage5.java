package arithmetic.arrays;

public class ArithmeticAvarage5 {

    public static void main(String[] args) {
        long d1, d2, d3, d4, d5;
        String[] x = new String[5];
        x[0] = args[0];
        d1 = Long.parseLong(x[0]);
        x[1] = args[1];
        d2 = Long.parseLong(x[1]);
        x[2] = args[2];
        d3 = Long.parseLong(x[2]);
        x[3] = args[3];
        d4 = Long.parseLong(x[3]);
        x[4] = args[4];
        d5 = Long.parseLong(x[4]);
        long average = (d1 + d2 + d3 + d4 + d5) / 5;
        System.out.printf("%s %d", "Arithmetic average: ", average);
    }

}
