package arithmetic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Numb5Parser {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        int d1, d2, d3, d4, d5;
        char c1, c2, c3, c4, c5;
        System.out.println("Input a 5 digit number: ");
        input = reader.readLine();
        d1 = input.charAt(0);
        c1 = (char) d1;
        d2 = input.charAt(1);
        c2 = (char) d2;
        d3 = input.charAt(2);
        c3 = (char) d3;
        d4 = input.charAt(3);
        c4 = (char) d4;
        d5 = input.charAt(4);
        c5 = (char) d5;
        System.out.printf("%c %c %c %c %c", c1, c2, c3, c4, c5);

    }

}
