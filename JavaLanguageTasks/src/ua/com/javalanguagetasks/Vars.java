package ua.com.javalanguagetasks;

public class Vars {

    public static void main(String[] args) {
        final String my_Name = "Vlad";
        byte age = 27;
        final short earth_Diameter = 12742;
        byte outsideTemperature = 11;
        int notDate = 27052020;
        final double electron_Mass = 9.10938356e-31;
        long CanisMajorDwarfGalaxy = 23600000000000000L;
    }

}
