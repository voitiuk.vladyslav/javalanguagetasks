import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class QuadraticEquationSolver {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("a=");
        String input = reader.readLine();
        double a = Double.parseDouble(input);
        if (a != 0) {
            System.out.print("b=");
            input = reader.readLine();
            double b = Double.parseDouble(input);
            System.out.print("c=");
            input = reader.readLine();
            double c = Double.parseDouble(input);
            double d = b * b - 4 * a * c;
            if (d == 0) {
                double x1 = (-1 * (b)) / (2 * a);
                System.out.println("x=" + x1);
            } else if (d > 0) {
                double x1 = (-1 * (b) + Math.sqrt(d)) / (2 * a);
                double x2 = (-1 * (b) - Math.sqrt(d)) / (2 * a);
                System.out.print("x1=" + x1 + "\n" + "x2=" + x2);
            } else {
                System.out.println("There is no solutions.");
            }
        } else {
            System.out.println("There is no solutions.");
        }
    }
}
