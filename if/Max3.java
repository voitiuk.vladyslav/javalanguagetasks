import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Max3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("First digit: ");
        String input = reader.readLine();
        int a = Integer.parseInt(input);
        System.out.print("Second digit: ");
        input = reader.readLine();
        int b = Integer.parseInt(input);
        System.out.print("Third digit: ");
        input = reader.readLine();
        int c = Integer.parseInt(input);
        if (a > b) {
            if (a > c) {
                System.out.println(a + " digit is biggest.");
            } else {
                System.out.println(c + " digit is biggest.");
            }
        } else if (b > c) {
            System.out.println(b + " digit is biggest.");
        } else {
            System.out.println(c + " digit is biggest.");
        }
    }
}
