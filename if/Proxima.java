import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Proxima {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Digit A: ");
        String input = reader.readLine();
        int d1 = Integer.parseInt(input);
        System.out.print("Digit B: ");
        input = reader.readLine();
        int d2 = Integer.parseInt(input);
        int c = 6;
        int d3 = (d1 - c) * (d1 - c);
        int d4 = (d2 - c) * (d2 - c);
        if (d3 < d4) {
            System.out.println("Point A is closest to C");
        } else {
            System.out.println("Point B is closest to C");
        }
    }
}

