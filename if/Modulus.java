public class Modulus {
    public static void main(String[] args) {
        String argument = args[0];
        int number = Integer.parseInt(argument);
        if (number % 2 == 0) {
            System.out.println("The number is even.");
        } else {
            System.out.println("The number is odd.");
        }
    }
}
