public class DayOfWeek {
    public static void main(String[] args) {
        String argument = args[0];
        int number = Integer.parseInt(argument);
        if (number == 1) {
            System.out.println("Today is Monday.");
        } else if (number == 2) {
            System.out.println("Today is Tuesday");
        } else if (number == 3) {
            System.out.println("Today is Wednesday.");
        } else if (number == 4) {
            System.out.println("Today is Thursday.");
        } else if (number == 5) {
            System.out.println("Today is Friday.");
        } else if (number == 6) {
            System.out.println("Today is Saturday.");
        } else if (number == 7) {
            System.out.println("Today is Sunday.");
        } else {
            System.out.println("DUDE. THERE ARE ONLY 7 DAYS IN A WEEK.");
        }
    }
}
