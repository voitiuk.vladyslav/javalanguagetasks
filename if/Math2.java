import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Math2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        System.out.print("Input first digit: ");
        input = reader.readLine();
        int d1 = Integer.parseInt(input);
        System.out.print("Input second digit: ");
        input = reader.readLine();
        int d2 = Integer.parseInt(input);
        if (d1 > d2) {
            System.out.println(d1 + " is bigger");
        } else {
            System.out.println(d2 + " is bigger");
        }
    }
}
