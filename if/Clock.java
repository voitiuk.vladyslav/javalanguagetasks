public class Clock {
    public static void main(String[] args) {
        String argument = args[0];
        int n = Integer.parseInt(argument);
        if (n >= 0 && n <= 28800) {
            int d1 = n / 3600;
            double d2 = n / 3600;
            System.out.println("Petrov " + n + " seconds");
            if (d2 >= 7 && d2 <= 8) {
                System.out.println("There is less then an hour left.");
            } else {
                if (d1 >= 0 && d1 <= 6) {
                    System.out.println("There are " + d1 + " hours passed.");
                }
            }
        } else {
            System.out.println("Work shift cant be longer then 8 hours.");
        }
    }
}
