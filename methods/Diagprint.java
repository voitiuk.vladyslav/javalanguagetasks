import java.util.Scanner;

public class Diagprint {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String input = scan.nextLine();
        diagonalPrint(input);
    }

    public static void diagonalPrint(String word) {
        for (int i = 0; i < word.length(); i++) {
            for (int counter = 0; counter < i; counter++) {
                System.out.print(" ");
            }
            System.out.println(word.charAt(i));
        }
    }
}
