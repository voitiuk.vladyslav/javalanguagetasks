import java.util.Arrays;

public class Calcs {
    public static void main(String[] args) {
        String input = args[0];
        result(input);
        System.out.println(result(input));
    }

    public static int result(String input) {
        int result = 0;
        int i = 0;
        while (result == 0) {
            if (input.charAt(i) == '+') {
                result = add(input);
            } else if (input.charAt(i) == '-') {
                result = sub(input);
            } else if (input.charAt(i) == '*') {
                result = multi(input);
                i++;
            } else if (input.charAt(i) == '/') {
                result = div(input);
                i++;
            } else if (input.charAt(i) == '%') {
                result = mod(input);
                if (result == 0) {
                    return result;
                }
                i++;
            } else if (input.charAt(i) == 's') {
                result = sqr(input);
                i++;
            } else {
                i++;
            }
        }
        return result;
    }

    public static int add(String input) {
        String regex = "\\+";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[0]);
        int b = Integer.parseInt(array[1]);
        return a + b;
    }

    public static int sub(String input) {
        String regex = "-";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[0]);
        int b = Integer.parseInt(array[1]);
        return a - b;
    }

    public static int multi(String input) {
        String regex = "\\*";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[0]);
        int b = Integer.parseInt(array[1]);
        return a * b;
    }

    public static int div(String input) {
        String regex = "\\\\/";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[0]);
        int b = Integer.parseInt(array[1]);
        return a / b;
    }

    public static int mod(String input) {
        String regex = "%";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[0]);
        int b = Integer.parseInt(array[1]);
        return a % b;
    }

    public static int sqr(String input) {
        String regex = "sqr";
        String array[] = input.split(regex);
        int a = Integer.parseInt(array[1]);
        a = a * a;
        return a;
    }
}
