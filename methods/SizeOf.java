public class SizeOf {
    public static void main(String[] args) {
        int[] array = new int[]{12, 2, 5, 67, 7, 23, 78};
        System.out.println("Size of integer array: " + integerSize(array));
        System.out.println("Size of byte array: " + byteSize(array));
        System.out.println("Size of short array: " + shortSize(array));
        System.out.println("Size of long array: " + longSize(array));
        System.out.println("Size of float array: " + floatSize(array));
        System.out.println("Size of double array: " + doubleSize(array));

    }

    public static int integerSize(int[] array) {
        int size = 4;
        int length = array.length;
        return size * length;
    }

    public static int doubleSize(int[] array) {
        int size = 8;
        int length = array.length;
        return size * length;
    }

    public static int longSize(int[] array) {
        int size = 8;
        int length = array.length;
        return size * length;
    }

    public static int floatSize(int[] a) {
        int size = 4;
        int length = a.length;
        return size * length;
    }

    public static int byteSize(int[] b) {
        int size = 1;
        int length = b.length;
        return size * length;
    }

    public static int shortSize(int[] c) {
        int size = 2;
        int length = c.length;
        return size * length;
    }
}
