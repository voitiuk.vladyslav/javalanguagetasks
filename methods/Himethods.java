import java.util.Arrays;
import java.util.Scanner;

public class Himethods {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int array[] = new int[]{12, 15, 234, 40};
        String a = input.next();
        int b = input.nextInt();
        double c = input.nextDouble();
        String result = s(a, b, c);
        System.out.println(result);
        System.out.println("Sum: " + arrayElementsSum(array) + "\n" + Arrays.toString(array));
    }

    public static String s(String a, int b, double c) {
        String s = a + " " + b + " " + c;
        return s;
    }

    public static int arrayElementsSum(int[] array) {
        int sum = 0;
        for (int i = 0; i < 3; i++) {
            array[i] = array[i] + 1;
        }
        for (int j = 0; j < array.length; j++) {
            sum = sum + array[j];
        }
        return sum;
    }
}

