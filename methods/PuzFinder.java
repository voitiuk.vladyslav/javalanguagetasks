import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.Scanner;

public class PuzFinder {
    public static void main(String[] args) {
        char[][] mainArray = new char[alphabetArrayGeneration().length][alphabetArrayGeneration().length];
        mainArray = alphabetArrayGeneration();
        for (int i = 0; i < mainArray.length; i++) {
            for (int j = 0; j < mainArray.length; j++) {
                System.out.print(mainArray[i][j] + "  ");
            }
            System.out.println();
        }
        Scanner input = new Scanner(System.in);
        String inputWord = input.nextLine();
        searchInput(inputWord, mainArray);
    }

    public static char rndChar() {
        int random = (int) (Math.random() * 26);
        char letter = (random < 26) ? 'a' : 'z';
        return (char) (letter + random % 26);
    }

    public static char[][] alphabetArrayGeneration() {
        char[][] alphabetArray = new char[8][8];
        for (int i = 0; i < alphabetArray.length; i++) {
            for (int j = 0; j < alphabetArray.length; j++) {
                char letter = rndChar();
                alphabetArray[i][j] = letter;
            }
        }
        return alphabetArray;
    }

    public static void searchInput(String inputWord, char[][] randomArray) {
        char[] inputArray = new char[inputWord.length()];
        inputArray = inputWord.toCharArray();
        String result;
        for (int i = 0; i < alphabetArrayGeneration().length; i++) {
            for (int j = 0; j < alphabetArrayGeneration().length; j++) {
                int counter = 0;
                if (Character.valueOf(randomArray[i][j]) ==
                        Character.valueOf(inputArray[counter])) {
                    result = "[" + String.valueOf(i) + "," + String.valueOf(j) + "]";
                    j++;
                    counter++;
                    if (Character.valueOf(randomArray[i][j]) ==
                            Character.valueOf(inputArray[counter])) {
                        j++;
                        counter++;
                        if (Character.valueOf(randomArray[i][j]) ==
                                Character.valueOf(inputArray[counter])) {
                            result = result + " Word placed horizontally";
                            System.out.println(result);
                            break;
                        }
                    }
                }
                if (Character.valueOf(randomArray[i][j]) ==
                        Character.valueOf(inputArray[counter])) {
                    result = "[" + String.valueOf(i) + "," + String.valueOf(j) + "]";
                    i++;
                    counter++;
                    if (Character.valueOf(randomArray[i][j]) ==
                            Character.valueOf(inputArray[counter])) {
                        i++;
                        counter++;
                        if (Character.valueOf(randomArray[i][j]) ==
                                Character.valueOf(inputArray[counter])) {
                            result = result + " Word placed vertically";
                            System.out.println(result);
                            break;
                        }
                    }
                }
            }
        }
    }
}


